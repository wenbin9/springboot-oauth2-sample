package com.sample.oauth2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.security.Principal;
/**
 * REST controller for managing the current user's account.
 */
@RestController
public class AccountResource {
    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    @GetMapping("/account")
    public String getAccount(Principal principal) {
        log.info("current login user: {}", principal.getName());
        return "您好," + principal.getName();
    }
}