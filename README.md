# springboot-oauth2-sample

#### 介绍
Spring Boot项目使用Oauth2协议集成Gitee登录

#### 使用说明

1.  application.yml中替换为自己的client-id和client-secret
2.  启动项目：mvn spring-boot:run
3.  访问 http://localhost:8080/login 进入登录页面进行登录
4.  登录成功后访问 http://localhost:8080/account 获取登录的用户名

